# happy-binded-keyboard

### concept

 stressless keyboard array and modes.

- mode implemantation
  - HHKB mode
  - Emacs mode
  - Lisp mode
  - and language optimized modes ...
- Daemon and GUI gadget on X11.





### references

東プレ HHKB 製品サイト
- HHKB Professional2 Type-S 白／無刻印 （英語配列）[Happy Hacking Keyboard | HHKB Professional2 Type-S | PFU](https://www.pfu.fujitsu.com/hhkeyboard/type-s/#)
Alternative controller 掲示板
- Alternative controller [[TMK] FC660C Alt Controller](https://geekhack.org/index.php?topic=90317.0)
書き込みについて
- [Home · tmk/tmk_keyboard Wiki](https://github.com/tmk/tmk_keyboard/wiki#flash-firmware)
キーマップエディタ
- [TMK Keymap Editor](http://www.tmk-kbd.com/tmk_keyboard/editor/unimap/?hhkb_rn42)






